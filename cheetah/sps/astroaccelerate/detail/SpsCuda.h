/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_SPS_ASTROACCELERATE_SPSCUDA_H
#define SKA_CHEETAH_SPS_ASTROACCELERATE_SPSCUDA_H

#include "cheetah/sps/astroaccelerate/Config.h"
#include "cheetah/sps/Config.h"
#include "cheetah/utils/Mock.h"

#ifdef ENABLE_ASTROACCELERATE //Note: This should probably be ENABLE_ASTROACCELERATE

#include "cheetah/ddtr/DedispersionConfig.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "panda/AggregationBufferFiller.h"
#include "panda/arch/nvidia/DeviceCapability.h"
#include "astro-accelerate/astro-accelerate/DedispersionStrategy.h"
#include "astro-accelerate/astro-accelerate/AstroAccelerate.h"
#include <tuple>
#include <vector>
#include <functional>
#include <memory>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

/**
 * @brief Single pulse search asynchronous task for CUDA
 *
 */

class SpsCuda : private utils::AlgorithmBase<Config, sps::Config>
{
    private:
        typedef utils::AlgorithmBase<Config, sps::Config> BaseT;

    public:
        // mark the architecture this algo is designed for
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;

    private:
        typedef uint8_t DataType;

    public:
        typedef data::TimeFrequency<Cpu> TimeFrequencyType;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    private:
        typedef typename ska::panda::AggregationBufferFiller<TimeFrequencyType>::AggregationBufferType BufferType;

    public:
        SpsCuda(sps::Config const& config);
        SpsCuda(SpsCuda&&) = default;
        SpsCuda(SpsCuda const&) = delete;
        ~SpsCuda();

        /**
         * @brief call the dedispersion/sps algorithm using the provided device
         */
        template<typename DmHandler, typename SpHandler>
        void operator()(panda::PoolResource<cheetah::Cuda>&, BufferType&, DmHandler&, SpHandler&);

        /**
         * @brief specify parameters to generate a suitable dedispersion compute strategy
         * @returns the minimum sample size required for the specified parameters
         */
        std::size_t set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const&);

        /**
         * @brief return the dedsipersion strategy object
         */
        ::astroaccelerate::DedispersionStrategy const& dedispersion_strategy() const;

        /**
         * @brief the number of time samples required to be copied from the end of the previous buffer
         *        in to the current one
         */
        std::size_t buffer_overlap() const;

    protected:

    private:
        std::size_t calculate_internals(TimeType);
        std::size_t _samples;
        std::vector<const ddtr::DedispersionConfig*> _dedisp_configs;
        float _threshold;
        std::unique_ptr<::astroaccelerate::DedispersionStrategy> _dedispersion_strategy;
        std::shared_ptr<::astroaccelerate::DmTime<float>> _dm_time;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        FrequencyType _chan_high;
        FrequencyType _chan_low;
};


} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

#else // ENABLE_ASTROACCELERATE

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

class SpsCuda : public utils::Mock<cheetah::Cuda, sps::Config const&>
{

    public:
        using utils::Mock<cheetah::Cuda, sps::Config const&>::Mock;

};

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

#endif // ENABLE_ASTROACCELERATE

#include "cheetah/sps/astroaccelerate/detail/SpsCuda.cpp"
#endif // SKA_CHEETAH_SPS_ASTROACCELERATE_SPSCUDA_H

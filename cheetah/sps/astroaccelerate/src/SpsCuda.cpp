/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/astroaccelerate/detail/SpsCuda.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

#ifdef ENABLE_ASTROACCELERATE

SpsCuda::SpsCuda(sps::Config const& config)
    : BaseT(config.astroaccelerate_config(), config)
{
    // Get the dedispersion plan
    auto it = config.subsection("dedispersion");
    while (it != config.subsection_end())
    {
        ddtr::DedispersionConfig const& c = static_cast<ddtr::DedispersionConfig const&>(*it);
        _dedisp_configs.push_back(&c);
        ++it;
    }
    // Get the detection threshold
    _threshold = config.threshold();
    _samples = config.dedispersion_samples();
}

SpsCuda::~SpsCuda()
{
}

std::size_t SpsCuda::buffer_overlap() const
{
    assert(_samples > _dedispersion_strategy->get_dedispersed_time_samples());
    return _samples - _dedispersion_strategy->get_dedispersed_time_samples();
}

::astroaccelerate::DedispersionStrategy const& SpsCuda::dedispersion_strategy() const
{
    return *_dedispersion_strategy;
}

// This should be pased a TimeFrequency object
std::size_t SpsCuda::set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const& tf_data)
{
    PANDA_LOG_DEBUG << "Setting SpsCuda dedispersion strategy. GPU memory=" << min_gpu_memory;
    std::size_t nranges = _dedisp_configs.size();
    if (nranges == 0)
    {
        throw panda::Error("No dedispersion plan set.");
    }
    std::vector<float> user_dm_low;
    std::vector<float> user_dm_high;
    std::vector<float> user_dm_step;
    std::vector<int> in_bin;
    std::vector<int> out_bin;

    for (auto& config: _dedisp_configs)
    {
        PANDA_LOG << "Configuring DM range: "
        << (float)config->dm_start().value() << " to "
        << (float)config->dm_end().value() << " pc/cc in steps of "
        << (float)config->dm_step().value() << " pc/cc";
        user_dm_low.push_back((float)config->dm_start().value());
        user_dm_high.push_back((float)config->dm_end().value());
        user_dm_step.push_back((float)config->dm_step().value());
        PANDA_LOG << "Disabling downsampling for DM range";
        in_bin.push_back(1);
        out_bin.push_back(1);
    }

    _chan_high = FrequencyType(0.0*boost::units::si::hertz);
    _chan_low = FrequencyType(std::numeric_limits<typename FrequencyType::value_type>::max()*boost::units::si::hertz);
    std::vector<float> chan_freqs;
    for (auto const& freq: tf_data.channel_frequencies())
    {
        if (freq > _chan_high)  
        {
            _chan_high = freq;
        }
        else if( freq < _chan_low )
        {
            _chan_low = freq;
        }
        chan_freqs.push_back(freq.value());
    }

    PANDA_LOG_DEBUG << "Generating new DedispersionStrategy object";
    PANDA_LOG_DEBUG << "User-requested samples to dedisperse: " << _samples;
    _dedispersion_strategy.reset( new ::astroaccelerate::DedispersionStrategy(
        user_dm_low
        , user_dm_high
        , user_dm_step
        , in_bin
        , out_bin
        , std::max(ArchitectureCapability::total_memory, min_gpu_memory) //gpu_memory
        , 8 //power
        ,(int) _dedisp_configs.size()
        ,_samples //nsamples
        ,1 //nifs -- not used by DedispersionStrategy
        ,8 //nbits -- not used by DedispersionStrategy
        ,tf_data.sample_interval().value() //tsamp (assumed to be in seconds)
        ,_threshold //sigma_cutoff
        ,6 //sigma_constant
        ,0.5 //max_boxcar_width_in_sec
        ,0 //narrow -- not used
        ,0 //wide -- not used
        ,0 //nboots -- not used
        ,0 //navdms -- not used
        ,0 //ntrial_bins -- not used
        ,0 //nsearch -- not used
        ,0 //aggression -- not used
        ,chan_freqs //chan frequency array
        ));

    return calculate_internals(tf_data.sample_interval());
}

std::size_t SpsCuda::calculate_internals(TimeType samp_interval)
{
    PANDA_LOG_DEBUG << "Calculating SpsCuda output sizes";

    _dm_time.reset(new ::astroaccelerate::DmTime<float>(*_dedispersion_strategy));

    std::size_t nranges = _dm_time->number_of_dm_ranges();

    std::size_t nsamps_processed = *std::max_element(_dm_time->nsamples().begin(), _dm_time->nsamples().end());

    _dm_trial_metadata.reset(new data::DmTrialsMetadata(samp_interval, nsamps_processed));

    for (std::size_t range=0; range < nranges; ++range)
    {
        std::size_t ndms = std::size_t(_dedispersion_strategy->get_ndms()[range]);
        data::DedispersionMeasureType<float> dm(_dedispersion_strategy->get_dm_low()[range] * data::parsecs_per_cube_cm);
        data::DedispersionMeasureType<float> dm_step(_dedispersion_strategy->get_dm_step()[range] * data::parsecs_per_cube_cm);
        std::size_t size = _dm_time->nsamples()[range];

        for (std::size_t dm_idx =0; dm_idx < ndms; ++dm_idx)
        {
            _dm_trial_metadata->emplace_back(dm,nsamps_processed/size);
            dm = dm + dm_step;
        }
    }

    return _samples;
}
#endif // ENABLE_ASTROACCELERATE

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

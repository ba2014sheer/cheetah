# FLDO Configuration Guide
The folding portion behavior can be changed by means of some
input parameters. In the following discussion the configurable parameter
names are in _italic_ . The complete list of SKA DDD input parameters for
PSS can be found in PSS_Parameters.md

FLDO receives multi-channel time-series data (a stream of filterbank data)
and produces dedispersed, binned (folded) profiles for each candidate in
input, at the given dispersion measure, period and period derivative.

Data are organized as a succession of measures, each comprising
_freq_channels_ samples each referring to a different frequency. Samples
are integer numbers _bit_per_sample_ bits long. The measures are taken
every _time_resolution_ microseconds. The number of measures is
_integration_time_ / _time_resolution_ .

The folding algorithm consists in a synchronous summation of input data in
order to improve the Signal/Noise.  Input data is processed in blocks of
_subints_ sub-integrations. Each sub-integration is then divided in
_sub_bands_ bands of frequencies and corrected for dispersion.
Data are summed up coherently, following the pulsar period of the current candidate in
an array of maximum length of _phases_ phases.

\image html subint_structure.png "Schema of data of a scan"
\image latex subint_structure.eps "Schema of data of a scan"

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CUDA_FLDO_CUH
#define SKA_CHEETAH_FLDO_CUDA_FLDO_CUH

#include "cheetah/fldo/cuda/Config.h"
#include "cheetah/fldo/cuda/detail/FldoCuda.h"
#include "cheetah/fldo/Config.h"
#include "cheetah/fldo/Types.h"
#include "cheetah/fldo/cuda/CommonDefs.h"

#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include <map>

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {


/**
 * @brief
 *    The interface for the CUDA FLDO algorithm
 * @details
 */

class Fldo
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapabilty; // minimum device requirements

        typedef panda::PoolResource<Architecture> ResourceType;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef data::TimeType TimeType;
        typedef boost::units::quantity<boost::units::si::time, int> ScanTimeType;
        typedef data::Candidate<Cpu, float> CandidateType;

    public:
        Fldo(fldo::Config const& config);
        ~Fldo();

        /**
         * @brief performs the folding operation on the provided data
         */
        std::shared_ptr<data::Ocld> operator()(ResourceType& device
                                              , std::vector<std::shared_ptr<data::TimeFrequency<Cpu>>> const& data
                                              , data::Scl const& input_candidates);

        /**
         * fldo_input_check(fldo::Config const& config)
         * @brief
         * verify input config parameter are inside the bounds defined in
         * CommonDefs.h.
         *
         * @return < 0 if error
         */
        int fldo_input_check(const fldo::Config & ) ;

    private:
        fldo::Config const& _config;
        std::map <unsigned, FldoCuda> _cuda_runner;
};

} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_FLDO_CUDA_FLDO_CUH

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/Fldo.h"
#include "cheetah/fldo/cuda/CommonDefs.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

#ifdef ENABLE_CUDA

Fldo::Fldo(fldo::Config const& config)
    : _config(config)
{
    //check on input data is done in fldo::Config
}

Fldo::~Fldo()
{
}

std::shared_ptr<data::Ocld> Fldo::operator()(ResourceType& gpu,
                                             std::vector<std::shared_ptr<data::TimeFrequency<Cpu>>> const& data,
                                             data::Scl const& candidates_list)
{

    auto it = this->_cuda_runner.find(gpu.device_id());
    if (it == this->_cuda_runner.end()) {
        if(_cuda_runner.count(gpu.device_id()) == 0) {
            //the device id is not an element of the map
            cudaDeviceProp deviceProp = gpu.device_properties();
            if (!deviceProp.canMapHostMemory) {
                std::stringstream err_msg;
                //err_msg << "Device " << gpu.device_id() << " cannot map host memory!";
                PANDA_LOG_ERROR << "Device " << gpu.device_id() << " cannot map host memory!";
                //throw a runtime exception
                throw panda::Error(err_msg.str());
            } else {
                CUDA_ERROR_CHECK(cudaSetDeviceFlags(cudaDeviceMapHost));
            }
            _cuda_runner.insert(std::make_pair(gpu.device_id(), FldoCuda(_config)));
        }
        it = this->_cuda_runner.find(gpu.device_id());
    }
    return (*it).second(gpu, data, candidates_list);
}

#endif //ENABLE_CUDA

} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

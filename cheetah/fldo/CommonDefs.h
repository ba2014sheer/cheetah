/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @brief
 *   Some limits and constants for FLDO.
 * @details
 *   Limits cames from TM-CSP interface definition. The current values are
 *   taken from 4/2017 version of 300-000000-021_01_CSPMID document.
 *   A table with limits is in doc directory
 *
 *   k_dm is the dispersion constant (e^2/(2 * PI * m_e * c)
 *
 */


#ifndef SKA_CHEETAH_FLDO_COMMONDEF_H
#define SKA_CHEETAH_FLDO_COMMONDEF_H

namespace ska {
namespace cheetah {
namespace fldo {

static constexpr size_t min_subints         =  32;
static constexpr size_t max_subints         =  256;
static constexpr size_t o_subints           =  64;

// intermediate data definitions
static constexpr size_t min_phases          =  16;
static constexpr size_t max_phases          =  256;
static constexpr size_t o_phases            =  128;
static constexpr size_t min_bands           =  64;
static constexpr size_t max_bands           =  128;
static constexpr size_t o_bands             =  min_bands;

static constexpr double k_dm                =  4.1493775933609e15; // dispersion constant (Hz^2 pc ^ (-1) cm ^3 s)


} // namespace fldo
} // namespace cheetah
} // namespace ska


#endif // SKA_CHEETAH_FLDO_COMMONDEF_H

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/test/ConfigTest.h"
#include "cheetah/fldo/Config.h"
#include "cheetah/fldo/CommonDefs.h"


namespace ska {
namespace cheetah {
namespace fldo {
namespace test {


ConfigTest::ConfigTest()
    : ::testing::Test()
{
}

ConfigTest::~ConfigTest()
{
}

void ConfigTest::SetUp()
{
}

void ConfigTest::TearDown()
{
}

TEST_F(ConfigTest, test_pool)
{
    pipeline::ScanConfig scan_config;
    utils::Config::SystemType system;
    typedef panda::PoolManagerConfig<utils::Config::SystemType> PoolManagerConfigType;
    PoolManagerConfigType pool_manager_config;
    Config::PoolManagerType manager(system, pool_manager_config);
    fldo::ConfigType config(manager, scan_config);
    auto pool = config.pool();
}

//
// test_correct_config
// This test programs a correct parameter Configuration
// frequecies.
TEST_F(ConfigTest, test_correct_config)
{
    pipeline::ScanConfig scan_config;
    Config config(scan_config);
    PANDA_LOG_DEBUG  << "ConfigTest,, test_correct_config";
    // correct values
    config.phases(fldo::o_phases);
    config.nsubbands(fldo::o_bands);
    config.nsubints(fldo::o_subints);
    //
    int res = config.fldo_input_check(config);
    ASSERT_EQ(0 , res );
}

//
// test_wrong_config
// This test programs a correct parameter Configuration
// frequecies.
TEST_F(ConfigTest, test_wrong_config)
{
    pipeline::ScanConfig scan_config;
    Config config(scan_config);
    // correct values
    config.phases(fldo::o_phases);
    config.nsubbands(fldo::o_bands);
    config.nsubints(fldo::max_subints + 1);
    ASSERT_GT(0 , config.fldo_input_check(config) );
}



} // namespace test
} // namespace fldo
} // namespace cheetah
} // namespace ska

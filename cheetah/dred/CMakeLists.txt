SUBPACKAGE(cuda)

set(module_dred_lib_src_cpu
    src/Config.cpp
    ${lib_src_cpu}
    PARENT_SCOPE
)

set(module_dred_lib_src_cuda
    ${lib_src_cuda}
    PARENT_SCOPE
)

TEST_UTILS()
add_subdirectory(test)

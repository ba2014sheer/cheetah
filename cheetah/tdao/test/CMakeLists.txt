include_directories(${GTEST_INCLUDE_DIR})

set(
    gtest_tdao_src_cpu
    src/gtest_tdao.cpp
  )

set(
    gtest_tdao_src_cuda
  )


if(ENABLE_CUDA)
  cuda_add_executable(gtest_tdao ${gtest_tdao_src_cuda} ${gtest_tdao_src_cpu})
else(ENABLE_CUDA)
  add_executable(gtest_tdao ${gtest_tdao_src_cpu})
endif(ENABLE_CUDA)

target_link_libraries(gtest_tdao ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_tdao gtest_tdao)

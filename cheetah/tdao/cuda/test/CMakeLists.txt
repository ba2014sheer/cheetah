include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(
   gtest_tdao_cuda_src
   src/TdaoTest.cu
   src/gtest_tdao_cuda.cu
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_tdao_cuda ${gtest_tdao_cuda_src})
    target_link_libraries(gtest_tdao_cuda ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
    add_test(gtest_tdao_cuda gtest_tdao_cuda --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
endif(ENABLE_CUDA)

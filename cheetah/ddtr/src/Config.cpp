/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/Config.h"
#include <algorithm>
#include <cmath>

namespace ska {
namespace cheetah {
namespace ddtr {

static std::string const dedispersion_tag("dedispersion");

Config::Config()
    : utils::Config("ddtr")
    , _dm_constant(data::dm_constant::s_mhz::dm_constant)
{
    add_factory(dedispersion_tag, []()
    {
        return new ddtr::DedispersionConfig();
    });
    add(_fpga_config);
    add(_cpu_config);
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("dm_constant", boost::program_options::value<double>()->default_value(_dm_constant.value())->notifier([this](double v) { _dm_constant = v * data::dm_constant::s_mhz_squared_cm_cubed_per_pc; }), "the dedispersion constant to use (in MHz^2 sec cm^3 per parsec");

    
}

std::vector<Config::Dm> Config::dm_trials() const
{
    std::vector<Dm> output;
    auto it = subsection(dedispersion_tag);
    while (it != subsection_end())
    {
        ddtr::DedispersionConfig const& c = static_cast<ddtr::DedispersionConfig const&>(*it);
        for (Dm dm = c.dm_start(); dm <= c.dm_end(); dm += c.dm_step())
        {
           output.push_back(dm);
        }
        ++it;
    }
    return output;
}

void Config::add_dm_range(Dm start, Dm end, Dm step)
{
    DedispersionConfig& dm_config = static_cast<DedispersionConfig&>(*generate_section(dedispersion_tag));
    dm_config.dm_start(start);
    dm_config.dm_end(end);
    dm_config.dm_step(step);
}

Config::DmConstantType Config::dm_constant() const
{
    return _dm_constant;
}

void Config::dm_constant(Config::DmConstantType dm_const)
{
    _dm_constant = dm_const;
}

fpga::Config const& Config::fpga_algo_config() const
{
    return _fpga_config;
}

fpga::Config& Config::fpga_algo_config()
{
    return _fpga_config;
}

cpu::Config const& Config::cpu_algo_config() const
{
    return _cpu_config;
}

cpu::Config& Config::cpu_algo_config()
{
    return _cpu_config;
}


} // namespace ddtr
} // namespace cheetah
} // namespace ska

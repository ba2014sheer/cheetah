/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_DEDISPERSION_H
#define SKA_CHEETAH_PIPELINE_DEDISPERSION_H
#include "cheetah/pipeline/RfiDetectionPipeline.h"
#include "cheetah/sps/Sps.h"
#include "cheetah/tdas/Tdas.h"
#include "cheetah/psbc/Psbc.h"
#include "cheetah/sift/Sift.h"
#include "cheetah/fldo/Fldo.h"
#include <panda/DataSwitch.h>
#include <memory>

namespace ska {
namespace cheetah {
namespace pipeline {
template<typename NumericalT>
class CheetahConfig;

/**
 * @brief
 *    The dedispersion pipeline
 * @details
 *
 */
//template<acceleration search>
template<typename NumericalT>
class Dedispersion : public PipelineHandler<NumericalT>
{
    private:
        typedef PipelineHandler<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;
        typedef sps::Sps<sps::ConfigType<typename CheetahConfig<NumericalT>::PoolManagerType>> Sps;
        typedef tdas::DmTrialsType DmTrialsType; // need to be able to switch pipelines on/off at runtime
        typedef typename Sps::SpType SpType;

    public:
        Dedispersion(CheetahConfig<NumericalT> const& config, BeamConfig const&);
        virtual ~Dedispersion();

        /**
         * @brief called whenever data is available for processing
         */
        void operator()(TimeFrequencyType&) override;

        /**
         * @brief reset the pipeline ready for the next observation run
         */
        void do_folding();

    private:
        struct FldoHandler {
            FldoHandler(Dedispersion&);
            void operator()(std::shared_ptr<data::Ocld>) const;

            private:
                DataExport<NumericalT>& _out;
        };

        struct SiftHandler {
            public:
                SiftHandler(Dedispersion&);
                void operator()(std::shared_ptr<data::Scl>) const;
            private:
                Dedispersion& _pipeline;
        };

    private:
        CheetahConfig<NumericalT> const& _config;
        FldoHandler _fldo_handler;
        fldo::Fldo<FldoHandler> _fldo;
        SiftHandler _sift_handler;
        sift::Sift<SiftHandler> _sift;
        tdas::Tdas<float, decltype(_sift)> _tdas;
        psbc::Psbc<decltype(_tdas)> _psbc;
        ska::panda::DataSwitch<DmTrialsType> _dm_switch; // need to be able to switch pipelines on/off at runtime
        ska::panda::DataSwitch<SpType> _sp_switch;

        Sps _sps;
        unsigned _first_sample_counter;
        std::vector<std::shared_ptr<TimeFrequencyType>> _tf_data;
        data::Scl _scl; // output from the acceleration search pipeline
};

} // namespace pipeline
} // namespace cheetah
} // namespace ska
#include "detail/Dedispersion.cpp"

#endif // SKA_CHEETAH_PIPELINE_DEDISPERSION_H

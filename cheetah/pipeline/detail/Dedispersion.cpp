/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/Dedispersion.h"
#include "cheetah/pipeline/CheetahConfig.h"
#include "panda/Log.h"
#include <iostream>

namespace ska {
namespace cheetah {
namespace pipeline {

template<typename NumericalT>
Dedispersion<NumericalT>::FldoHandler::FldoHandler(Dedispersion<NumericalT>& pipeline)
    : _out(pipeline.out())
{
}

template<typename NumericalT>
void Dedispersion<NumericalT>::FldoHandler::operator()(std::shared_ptr<data::Ocld> data) const
{
    (void) data;
    // TODO enable this when a Ocld exporter is available
    //_out.send("ocld", data);
}

template<typename NumericalT>
Dedispersion<NumericalT>::SiftHandler::SiftHandler(Dedispersion& pipeline)
    : _pipeline(pipeline)
{
}

template<typename NumericalT>
void Dedispersion<NumericalT>::SiftHandler::operator()(std::shared_ptr<data::Scl> scl) const
{
    _pipeline._scl += *scl;
}

template<typename NumericalT>
Dedispersion<NumericalT>::Dedispersion(CheetahConfig<NumericalT> const& config, BeamConfig const& beam_config)
    : BaseT(config, beam_config)
    , _config(config)
    , _fldo_handler(*this)
    , _fldo(config.fldo_config(), _fldo_handler)
    , _sift_handler(*this)
    , _sift(config.sift_config(), _sift_handler)
    , _tdas(config.tdas_config(), _sift)
    , _psbc(config.psbc_config(), _tdas)
    , _dm_switch(config.switch_config())
    , _sp_switch(config.switch_config())
    , _sps(config.sps_config(),
        [this](std::shared_ptr<DmTrialsType> data) {
            _dm_switch.send(panda::ChannelId("acc_search"), data);
        }
       ,[this](std::shared_ptr<SpType> data) {
            _sp_switch.send(panda::ChannelId("sps_events"), data);
        })
    , _first_sample_counter(0)
{
    _dm_switch.template add<DmTrialsType>(panda::ChannelId("acc_search"),
        [this](DmTrialsType& data) {
            // acc search pipeline
            // call the psbc operator()
            _psbc(data.shared_from_this());
        });
    if (!config.tdas_config().active())
    {
        _dm_switch.deactivate_all(panda::ChannelId("acc_search"));
    }
    _sp_switch.template add<SpType>(panda::ChannelId("sps_events"),
        [this](SpType& data) {
            // Single pulse candidates pipeline
            // Should go to SPOPT
	        this->out().send(panda::ChannelId("sps_events"), data);
        });
    _sp_switch.activate_all(panda::ChannelId("sps_events"));
}

template<typename NumericalT>
Dedispersion<NumericalT>::~Dedispersion()
{
    _config.pool_manager().wait();
    PANDA_LOG_DEBUG << "Dedispersion<NumericalT>::~Dedispersion()";
}

template<typename NumericalT>
void Dedispersion<NumericalT>::do_folding()
{
    _fldo(_tf_data, _scl);
    _tf_data.clear();
    _scl.clear();
}

template<typename NumericalT>
void Dedispersion<NumericalT>::operator()(TimeFrequencyType& chunk)
{
    chunk.first_sample_offset(_first_sample_counter);
    _tf_data.push_back(chunk.shared_from_this());
    _sps(chunk);
    _first_sample_counter += chunk.number_of_spectra();
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska

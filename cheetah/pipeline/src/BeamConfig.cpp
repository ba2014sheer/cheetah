/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/BeamConfig.h"
#include <iostream>

namespace ska {
namespace cheetah {
namespace pipeline {


BeamConfig::BeamConfig(std::string const& tag)
    : BaseT(tag)
    , _active(true)
{
    // setup data source options
    add(_thread_config);
    add(_data_src_config);
    add(_data_config);

    _data_src_config.add(_sigproc_config);
    _data_src_config.add(_psrdada_config);
    _data_src_config.add(_rcpt_config);
}

BeamConfig::~BeamConfig()
{
}

DataConfig const& BeamConfig::data_config() const
{
    return _data_config;
}

sigproc::Config const& BeamConfig::sigproc_config() const
{
    return _sigproc_config;
}

psrdada::Config const& BeamConfig::psrdada_config() const
{
    return _psrdada_config;
}

rcpt::Config const& BeamConfig::rcpt_config() const
{
    return _rcpt_config;
}

panda::ThreadConfig const& BeamConfig::thread_config() const
{
    return _thread_config;
}

void BeamConfig::thread_config(panda::ThreadConfig const& thread_config)
{
    _thread_config = thread_config;
}

void BeamConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::bool_switch()->default_value(_active)->notifier([&](bool val) { _active = val; }), "enable this beam");
}

bool BeamConfig::active() const
{
    return _active;
}

void BeamConfig::active(bool active)
{
    _active = active;
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska

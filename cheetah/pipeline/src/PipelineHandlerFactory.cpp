/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/PipelineHandlerFactory.h"
#include "cheetah/pipeline/CheetahConfig.h"
#include <utility>
#include <type_traits>

// included pipelines
#include "cheetah/pipeline/Empty.h"
#include "cheetah/pipeline/Dedispersion.h"
#include "cheetah/pipeline/SinglePulse.h"
#include "cheetah/pipeline/RfiDetectionPipeline.h"

#include "panda/Error.h"
#include "panda/MixInTimer.h"

namespace ska {
namespace cheetah {
namespace pipeline {

template<typename T, typename NumericalRep>
static
T* make_handler(CheetahConfig<NumericalRep> const& config, BeamConfig const& beam_config) {
    return new T(config, beam_config);
}

// wrapper to protect the pipeline from being destroyed before tasks in the pools finish
template<typename NumericalRep, typename Base>
class PipelineWrapper : public Base
{
    public:
        template<typename... Args>
        PipelineWrapper(CheetahConfig<NumericalRep> const& config, Args&&... args)
            : Base(config, std::forward<Args>(args)...)
            , _config(config)
        {
        }

        PipelineWrapper()
        {
            _config.pool_manager().wait();
        }

        void operator()(data::TimeFrequency<Cpu, NumericalRep>& data) override
        {
            this->Base::operator()(data);
        }

    private:
        CheetahConfig<NumericalRep> const& _config;
};

// wrapper to implement required virtual function of PipelineHandler
template<typename NumericalRep, typename Base>
class TimerWrapper : public PipelineWrapper<NumericalRep, panda::MixInTimer<Base>>
{
        typedef PipelineWrapper<NumericalRep, panda::MixInTimer<Base>> BaseT;

    public:
        template<typename... Args>
        TimerWrapper(Args&&... args)
            : BaseT(std::forward<Args>(args)...)
        {
        }

        void operator()(data::TimeFrequency<Cpu, NumericalRep>& data) override
        {
            this->BaseT::operator()(data);
        }
};

#define REGISTER_HANDLER(handler_name) \
    add_type(#handler_name, make_handler<handler_name<NumericalRep>, NumericalRep>)

PipelineHandlerFactory::PipelineHandlerFactory(CheetahConfig<NumericalRep>& config)
    : _config(config)
{
    // add known handlers
    REGISTER_HANDLER(Empty);
    REGISTER_HANDLER(Dedispersion);
    REGISTER_HANDLER(SinglePulse);
    REGISTER_HANDLER(RfiDetectionPipeline);

    config.set_pipeline_handlers(available());
}

PipelineHandlerFactory::~PipelineHandlerFactory()
{
}

template<typename TypeFactory>
void PipelineHandlerFactory::add_type(std::string handler_name, TypeFactory /*factory*/)
{
    _types.push_back(handler_name);
    //_map.insert(std::make_pair(handler_name, static_cast<PipelineHandlerFactory::FactoryType>(factory)));
    // add a timer type
    typedef typename std::remove_pointer<decltype(std::declval<TypeFactory>()(_config, std::declval<BeamConfig const&>()))>::type Type;
    _map.insert(std::make_pair(handler_name, &make_handler<PipelineWrapper<NumericalRep, Type>, NumericalRep>));
    _timed_map.insert(std::make_pair(handler_name, &make_handler<TimerWrapper<NumericalRep, Type>, NumericalRep>));
}

std::vector<std::string> PipelineHandlerFactory::available() const
{
    return _types;
}

PipelineHandlerFactory::HandlerType* PipelineHandlerFactory::create(std::string const& handler_name, BeamConfig const& beam_config) const
{
    auto it = _map.find(handler_name);
    if(it == _map.end())
        throw panda::Error("Pipeline handler '" + handler_name + "' unknown");

    return it->second(_config, beam_config);
}

PipelineHandlerFactory::HandlerType* PipelineHandlerFactory::create_timed(std::string const& handler_name, BeamConfig const& beam_config) const
{
    auto it = _timed_map.find(handler_name);
    if(it == _timed_map.end())
        throw panda::Error("Pipeline handler '" + handler_name + "' unknown");

    return it->second(_config, beam_config);
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska

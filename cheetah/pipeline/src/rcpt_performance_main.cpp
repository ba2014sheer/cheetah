/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/emulator/EmulatorAppConfig.h"
#include "cheetah/rcpt/PacketGenerator.h"
#include "cheetah/emulator/Factory.h"
#include "cheetah/emulator/Config.h"
#include "cheetah/rcpt/RcptTimerStats.h"
#include "cheetah/rcpt/UdpStream.h"
#include "panda/BasicAppConfig.h"
#include "panda/IpAddress.h"
#include "panda/TupleUtilities.h"
#include "panda/Log.h"
#include <memory>
#include <thread>
#include <chrono>
#include <type_traits>

//class PerformanceTesterConfig : public ska::panda::BasicAppConfig
class PerformanceTesterConfig : public ska::cheetah::emulator::EmulatorAppConfig
{
    public:
        PerformanceTesterConfig()
            : ska::cheetah::emulator::EmulatorAppConfig("rcpt_performance_test", "execute performance tests for the UDP rcpt data ingest of cheetah ")
            , _iterations(10000)
        {
            add(_rcpt_config);
        }

        ska::cheetah::rcpt::Config const& rcpt_config() const
        {
            return _rcpt_config;
        }

        ska::cheetah::rcpt::Config& rcpt_config()
        {
            return _rcpt_config;
        }

        unsigned iterations() const {
            return _iterations;
        }

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override
        {
            add_options
                ("iter", boost::program_options::value<unsigned>(&_iterations)->default_value(_iterations), "number of chunks to process for each test");
            EmulatorAppConfig::add_options(add_options);
        }


    private:
        ska::cheetah::rcpt::Config _rcpt_config;
        unsigned _iterations;
};

typedef std::chrono::high_resolution_clock ClockType;

int main(int argc, char** argv) {
    PerformanceTesterConfig app_config;
    int rv;
    typedef uint8_t DataType;
    try {
        // ---------- configuration -----------------
        ska::cheetah::emulator::Config& emulator_config = app_config.emulator_config();
        // add dynamic items to config
        ska::cheetah::generators::GeneratorFactory<DataType> generator_factory(emulator_config.generators_config());
        app_config.set_generator_list(generator_factory.available());

        // parse command line options
        if( (rv=app_config.parse(argc, argv)) ) return rv;

        // set the stream to listen on a random port
        auto& rcpt_config = app_config.rcpt_config();
        ska::panda::IpAddress address(0, "127.0.0.1");
        boost::asio::ip::udp::endpoint local_endpoint = address.end_point<boost::asio::ip::udp::endpoint>();
        rcpt_config.remote_end_point(local_endpoint);
        ska::cheetah::rcpt::UdpStream stream(rcpt_config);
        ska::panda::DataManager<ska::cheetah::rcpt::UdpStream> dm(stream);

        auto endpoint = stream.local_end_point();
        emulator_config.fixed_address(ska::panda::IpAddress(endpoint));

        // setup the emulator
        typedef ska::cheetah::rcpt::PacketGenerator<ska::cheetah::generators::TimeFrequencyGenerator<uint8_t>> StreamType;
        typedef ska::cheetah::emulator::Factory<StreamType, uint8_t> FactoryType;
        typedef typename FactoryType::EmulatorType EmulatorType;
        FactoryType factory(emulator_config, generator_factory);
        std::unique_ptr<EmulatorType> emulator(factory.create(emulator_config.generator()));
        PANDA_LOG << "emulator using generator: '" << emulator_config.generator() << "'";

        std::thread th([&]() { emulator->run(); });

        // ---------- tests -----------------
        ska::cheetah::rcpt::RcptTimerStats<ClockType> stats;
        unsigned iter_max = app_config.iterations();
        for(unsigned iter=0; iter < iter_max; ++iter)
        {
            auto data=dm.next();
            stats(*std::get<ska::panda::Index<std::shared_ptr<typename ska::cheetah::rcpt::BeamFormerDataTraits::DataType>, typename decltype(dm)::DataSetType>::value>(data), ClockType::now());
        }
        PANDA_LOG << "\n----\n\t" << stats << "\n----";
        emulator->stop();
        th.join();
    }
    catch(std::exception const& e) {
        std::cerr << "Emulator: " << e.what() << std::endl;
    }
    catch(...) {
        std::cerr << "Emulator: unknown exception caught" << std::endl;
    }
    return 1;
}

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SIFT_TEST_SIFTTESTER_H
#define SKA_CHEETAH_SIFT_TEST_SIFTTESTER_H

#include "cheetah/sift/Config.h"
#include "cheetah/sift/Sift.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "cheetah/data/Candidate.h"
#include "cheetah/data/Units.h"

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace sift {
namespace test {

/**
 * @brief
 * Generic functional test for the SiftTester algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requirements it needs to run.
 *
 * e.g.
 * @code
 * struct SimpleSiftTraits : public SiftTesterTraits<simple_sift::Sift::Architecture, simple_sift::Sift::ArchitectureCapability>
 * {
 *   typedef test::SiftTesterTraits<simple_sift::Sift::Architecture, typename simple_sift::Sift::ArchitectureCapability> BaseT;
 *   typedef Sift::Architecture Arch;
 *   typedef typename BaseT::DeviceType DeviceType;
 * };
 * @endcode
 *
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<SimpleSiftTraits, OpenClTraits> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, SiftTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */

template<typename ArchitectureTag, typename ArchitectureCapability>
struct SiftTesterTraits : public utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability>
{
    public:
        typedef utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability> BaseT;
        typedef ArchitectureTag Arch;
        typedef typename BaseT::DeviceType DeviceType;

    public:
        struct SiftHandler {
            void operator()(std::shared_ptr<data::Scl>) const;
        };

    public:
        SiftTesterTraits();
        sift::Sift<SiftHandler>& api();
        sift::Config& config();

    private:
        sift::Config _config;
        SiftHandler _handler;
        sift::Sift<SiftHandler> _api;
};

template <typename TestTraits>
class SiftTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        SiftTester();
        ~SiftTester();

    private:
};

TYPED_TEST_CASE_P(SiftTester);

} // namespace test
} // namespace sift
} // namespace cheetah
} // namespace ska

#include "cheetah/sift/test_utils/detail/SiftTester.cpp"

#endif // SKA_CHEETAH_SIFT_TEST_SIFTTESTER_H

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sift/Sift.h"

namespace ska {
namespace cheetah {
namespace sift {

template<typename Handler>
template <typename Arch>
std::shared_ptr<data::Scl> Sift<Handler>::operator()(panda::PoolResource<Arch>& resource, data::Ccl& input)
{
    auto& algo = _implementations.template get<Arch>();
    return algo.process(resource, input);
}

template<typename Handler>
Sift<Handler>::Sift(Config const& config, Handler handler)
    : _config(config)
    , _implementations(simple_sift::Sift(config.simple_sift_algo_config(), _config))
    , _handler(handler)
{
}

template<typename Handler>
Sift<Handler>::~Sift()
{
}

template<typename Handler>
void Sift<Handler>::operator()(std::shared_ptr<data::Ccl> const& input)
{
    // TODO use a pool
    auto& algo = _implementations.template get<cheetah::Cpu>();
    panda::PoolResource<cheetah::Cpu> cpu(0);

    _handler(algo.process(cpu, *input));
}

} // namespace sift
} // namespace cheetah
} // namespace ska

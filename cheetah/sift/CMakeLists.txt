SUBPACKAGE(simple_sift)

set(module_sift_lib_src_cpu
    src/Config.cpp
    src/Sift.cpp
    ${lib_src_cpu}
    PARENT_SCOPE
)

TEST_UTILS()
add_subdirectory(test)

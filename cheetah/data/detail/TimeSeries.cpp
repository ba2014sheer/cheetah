/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/TimeSeries.h"

namespace ska {
namespace cheetah {
namespace data {

template <typename Arch, typename ValueType, typename Alloc>
TimeSeries<Arch, ValueType, Alloc>::TimeSeries()
    : _sampling_interval(1.0 * second)
{
}

template <typename Arch, typename ValueType, typename Alloc>
TimeSeries<Arch, ValueType, Alloc>::TimeSeries(TimeType const& dt)
    : _sampling_interval(dt)
{
}

template <typename Arch, typename ValueType, typename Alloc>
TimeSeries<Arch, ValueType, Alloc>::~TimeSeries()
{
}

template <typename Arch, typename ValueType, typename Alloc>
TimeType const& TimeSeries<Arch, ValueType, Alloc>::sampling_interval() const
{
    return _sampling_interval;
}

template <typename Arch, typename ValueType, typename Alloc>
void TimeSeries<Arch, ValueType, Alloc>::sampling_interval(TimeType const& dt)
{
    _sampling_interval = dt;
}

} // namespace data
} // namespace cheetah
} // namespace ska

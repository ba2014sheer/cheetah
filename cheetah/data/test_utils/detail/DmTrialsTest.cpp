#include "cheetah/data/test_utils/DmTrialsTest.h"
#include "cheetah/data/test_utils/DmTrialsGeneratorUtil.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/Units.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

template <typename DmTrialsTestTraitsType>
DmTrialsTest<DmTrialsTestTraitsType>::DmTrialsTest()
    : ::testing::Test()
{
}

template <typename DmTrialsTestTraitsType>
DmTrialsTest<DmTrialsTestTraitsType>::~DmTrialsTest()
{
}

template <typename DmTrialsTestTraitsType>
void DmTrialsTest<DmTrialsTestTraitsType>::SetUp()
{
}

template <typename DmTrialsTestTraitsType>
void DmTrialsTest<DmTrialsTestTraitsType>::TearDown()
{
}

template <typename TypeParam>
void Tester<TypeParam>::size_test()
{
    typedef DmTrials<typename TypeParam::Architecture, typename TypeParam::ValueType> DmTrialsType;

    typedef typename DmTrialsType::TimeType TimeType;
    typedef typename DmTrialsType::DmType Dm;
    auto now = cheetah::utils::ModifiedJulianClock::now();
    auto metadata = data::DmTrialsMetadata::make_shared(TimeType(0.000064*data::seconds),1<<12);
    metadata->emplace_back(Dm(0.0*data::parsecs_per_cube_cm),1);
    metadata->emplace_back(Dm(10.0*data::parsecs_per_cube_cm),2);
    metadata->emplace_back(Dm(20.0*data::parsecs_per_cube_cm),4);
    DmTrialsType trials(metadata,now);
    ASSERT_EQ(trials.size(),std::size_t(3));
    ASSERT_EQ(trials[0].size(),std::size_t((1<<12)/1));
    ASSERT_EQ(trials[1].size(),std::size_t((1<<12)/2));
    ASSERT_EQ(trials[2].size(),std::size_t((1<<12)/4));
}

template <typename TypeParam>
void Tester<TypeParam>::iterator_test()
{
    typedef DmTrials<typename TypeParam::Architecture, typename TypeParam::ValueType> DmTrialsType;
    typedef typename DmTrialsType::TimeType TimeType;
    typedef typename DmTrialsType::DmType Dm;
    std::size_t count = 1<<12;
    DmTrialsGeneratorUtil<DmTrialsType> trials_generator;
    auto trials = trials_generator.generate(TimeType(0.000064*data::seconds),count,3);
    for (auto it=trials->begin(); it!=trials->end(); ++it)
    {
        ASSERT_EQ((*it).size(),count);
    }
}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska

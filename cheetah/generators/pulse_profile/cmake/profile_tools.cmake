# -- these macros generate the boilerplate for the ProfileManagerInitialiser class
# add profiles with the 
#    ADD_PROFILE(profile_name) 
# macro for files in the profle_data directory
# or the 
#    ADD_PROFILE_FROM_FILE(prfile_name data_file_name)
# for file elsewhere.
# When all profiles are added call the 
#   PROFILE_INIT()
# macro
# @example
#   ADD_PROFILE(profile_name_1)
#   ADD_PROFILE(profile_name_2)
#   PROFILE_INIT()
# @endexample

# generate a c++ PulsearProfile class from a data file
macro(ADD_PROFILE_FROM_FILE profile file_name)
    set(unmodified_profile_name ${profile})
    string(REGEX REPLACE "[+]" "_plus_" profile_name ${profile})
    set(class_name "Profile_${profile_name}")
    set(profile_header_file "${class_name}.h")
    set(profile_cpp_file "${class_name}.cpp")
    file(READ ${file_name} raw_profile_data)
    string(REGEX REPLACE "\n" "," profile_data "${raw_profile_data}" )
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/src/Profile.h.in ${CMAKE_CURRENT_BINARY_DIR}/${profile_header_file})
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/src/Profile.cpp.in ${CMAKE_CURRENT_BINARY_DIR}/${profile_cpp_file})
    list(APPEND lib_src_cpu ${profile_cpp_file})
    list(APPEND profile_includes ${profile_cpp_file})
    list(APPEND profile_classes ${class_name})
endmacro(ADD_PROFILE_FROM_FILE profile_name)

macro(ADD_PROFILE profile_name)
    ADD_PROFILE_FROM_FILE(${profile_name} "profile_data/${profile_name}.txt")
endmacro(ADD_PROFILE profile_name)

function(PROFILE_HEADER header_file initialiser_file)
    # -- the include file for initialising profiles
    set(header_file_gen "${header_file}.gen")
    file(WRITE ${header_file_gen} "// Generated file - do not edit\n")
    foreach(include_file ${profile_includes})
        file(WRITE ${header_file_gen} "#include \"${include_file}\"\n")
    endforeach(include_file)
    configure_file(${header_file_gen} ${header_file} COPYONLY)

    # -- the include file for initialising profiles
    set(initialiser_file_gen "${initialiser_file}.gen")
    foreach(class ${profile_classes})
        file(WRITE ${initialiser_file_gen} "        ${class}::add(manager);\n")
    endforeach(class)
    configure_file(${initialiser_file_gen} ${initialiser_file} COPYONLY)

endfunction(PROFILE_HEADER)

macro(PROFILE_INIT)
    set(profile_initialiser_cpp "ProfileManagerInitialiser.cpp")
    set(profile_include_file "${CMAKE_CURRENT_BINARY_DIR}/AddProfileInitilisationHeader.h")
    set(profile_init_file "${CMAKE_CURRENT_BINARY_DIR}/AddProfileInitialiser.cpp")
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/src/ProfileManagerInitialiser.cpp.in" "${CMAKE_CURRENT_BINARY_DIR}/${profile_initialiser_cpp}")
    list(APPEND lib_src_cpu ${profile_initialiser_cpp})
    PROFILE_HEADER(${profile_include_file} ${profile_init_file})
endmacro(PROFILE_INIT)

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt/BeamFormerPacket.h"
#include <cassert>


namespace ska {
namespace cheetah {
namespace rcpt {

template<std::size_t PayloadSize>
constexpr std::size_t BeamFormerPacket<PayloadSize>::header_size()
{
    return sizeof(Header);
}

template<std::size_t PayloadSize>
constexpr std::size_t BeamFormerPacket<PayloadSize>::footer_size()
{
    //return sizeof(Footer);
    return 0;
}

template<std::size_t PayloadSize>
constexpr std::size_t BeamFormerPacket<PayloadSize>::payload_size()
{
    return _payload_size;
}

template<std::size_t PayloadSize>
constexpr std::size_t BeamFormerPacket<PayloadSize>::number_of_samples()
{
    return _number_of_samples;
}

template<std::size_t PayloadSize>
constexpr std::size_t BeamFormerPacket<PayloadSize>::size()
{
    return _size;
}

template<std::size_t PayloadSize>
constexpr uint64_t BeamFormerPacket<PayloadSize>::max_sequence_number()
{
    return 0xffffffffffffff;
}

template<std::size_t PayloadSize>
BeamFormerPacket<PayloadSize>::BeamFormerPacket()
{
}

template<std::size_t PayloadSize>
BeamFormerPacket<PayloadSize>::~BeamFormerPacket()
{
}

template<std::size_t PayloadSize>
std::size_t BeamFormerPacket<PayloadSize>::number_of_channels()
{
    return _number_of_samples;
}

template<std::size_t PayloadSize>
void BeamFormerPacket<PayloadSize>::packet_count(uint64_t packet_count)
{
    _header._counter = static_cast<boost::endian::little_uint64_t>(packet_count);
}

template<std::size_t PayloadSize>
void BeamFormerPacket<PayloadSize>::packet_type(PacketType const type)
{
    _header._packet_type = static_cast<uint8_t>(type);
}

template<std::size_t PayloadSize>
PacketType BeamFormerPacket<PayloadSize>::packet_type() const
{
    return static_cast<PacketType>(_header._packet_type);
}

template<std::size_t PayloadSize>
uint64_t BeamFormerPacket<PayloadSize>::packet_count() const
{
    return static_cast<uint64_t>(_header._counter);
    //uint64_t counter = (*((uint64_t *) _header._counter));
    //return counter;
}

template<std::size_t PayloadSize>
void BeamFormerPacket<PayloadSize>::insert(std::size_t sample_number, Sample s)
{
    assert(sample_number < number_of_samples());
    _data[sample_number] = std::move(s); // seems to be faster than using std::swap
}

template<std::size_t PayloadSize>
Sample const& BeamFormerPacket<PayloadSize>::sample(std::size_t sample_number) const
{
    return _data[sample_number];
}

template<std::size_t PayloadSize>
const Sample* BeamFormerPacket<PayloadSize>::begin() const
{
    return &_data[0];
}

template<std::size_t PayloadSize>
const Sample* BeamFormerPacket<PayloadSize>::end() const
{
    return &_data[_payload_size/sizeof(Sample)];
}

template<std::size_t PayloadSize>
uint16_t BeamFormerPacket<PayloadSize>::first_channel_number() const
{
    return static_cast<uint16_t>(_header._first_channel_number);
}

template<std::size_t PayloadSize>
void BeamFormerPacket<PayloadSize>::first_channel_number(uint16_t number)
{
    _header._first_channel_number = static_cast<boost::endian::little_uint16_t>(number);
}

} // namespace rcpt
} // namespace cheetah
} // namespace ska

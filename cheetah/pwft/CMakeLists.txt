SUBPACKAGE(cuda)

set(module_pwft_lib_src_cuda
  ${lib_src_cuda}
  PARENT_SCOPE
  )

set(module_pwft_lib_src_cpu
  src/Config.cpp
  src/Pwft.cpp
  ${lib_src_cpu}
  PARENT_SCOPE
   )

TEST_UTILS()
add_subdirectory(test)

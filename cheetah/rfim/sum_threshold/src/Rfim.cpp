#include "cheetah/rfim/sum_threshold/Rfim.h"
#include "panda/Buffer.h"
#include "panda/Log.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#pragma GCC diagnostic pop
#include <cmath>


namespace ska {
namespace cheetah {
namespace rfim {
namespace sum_threshold {

typedef panda::Buffer<bool> FlagType;

Rfim::Rfim(const Config& config)
    : _config(config)
{
}

Rfim::~Rfim()
{
}

} // namespace sum_threshold
} // namespace rfim
} // namespace cheetah
} // namespace ska

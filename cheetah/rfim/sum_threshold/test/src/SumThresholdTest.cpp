#include "cheetah/rfim/sum_threshold/test/SumThresholdTest.h"
#include "cheetah/rfim/sum_threshold/Rfim.h"
#include "cheetah/rfim/test_utils/RfimTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace rfim {
namespace sum_threshold {
namespace test {

struct SumThresholdTraits : public rfim::test::RfimTesterTraits<rfim::sum_threshold::Rfim::Architecture> {
    typedef RfimTesterTraits<rfim::sum_threshold::Rfim::Architecture> BaseT;

    SumThresholdTraits();

    static ResultType& apply_algorithm(DeviceType&, DataType& data);

    rfim::sum_threshold::Config _config;
    bool _initialised;

    static std::unique_ptr<rfim::sum_threshold::Rfim> _algo;
};

SumThresholdTraits::SumThresholdTraits()
{
    // initialise the object
    _algo.reset(new rfim::sum_threshold::Rfim(_config));
}

SumThresholdTraits::ResultType& SumThresholdTraits::apply_algorithm(DeviceType& d, DataType& data)
{
    rfim::sum_threshold::Rfim* ptr = _algo.get();
    assert(ptr != nullptr);
    _algo->operator()(d, data.shared_from_this());
    return data;
}

std::unique_ptr<rfim::sum_threshold::Rfim> SumThresholdTraits::_algo = nullptr;


SumThresholdTest::SumThresholdTest()
    : ::testing::Test()
{
}

SumThresholdTest::~SumThresholdTest()
{
}

void SumThresholdTest::SetUp()
{
}

void SumThresholdTest::TearDown()
{
}

/*
TEST_F(SumThresholdTest, test_something)
{
}
*/


} // namespace test
} // namespace sum_threshold
namespace test {
    typedef ::testing::Types<rfim::sum_threshold::test::SumThresholdTraits> SumThresholdTraitsTypes;
    INSTANTIATE_TYPED_TEST_CASE_P(SumThreshold, RfimTester, SumThresholdTraitsTypes);
} // namespace test
} // namespace rfim
} // namespace cheetah
} // namespace ska

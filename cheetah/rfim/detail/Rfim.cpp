/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/Rfim.h"
#include "cheetah/rfim/ampp/Rfim.h"
#include "cheetah/rfim/RfimBlock.h"
#include "cheetah/rfim/cuda/Rfim.h"
#include "cheetah/rfim/sum_threshold/Rfim.h"
#include "panda/Error.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace rfim {

template<typename TimeFrequencyType, typename Handler, typename ConfigType>
Rfim<TimeFrequencyType, Handler, ConfigType>::Rfim(ConfigType const& c, Handler& handler)
    : _task(c.pool(), handler)
{
    // define the tasks to use from the configuration
    // n.b. ensure only one algoritm is assigned to any particular type of device
    if(c.sum_threshold_algo_config().active() && c.ampp_algo_config().active()) {
        static const std::string msg("rfim:: ambiguous CPU based algorithm - please deactive either ampp or sum_threshold");
        std::cerr << msg  << std::endl;
        throw panda::Error(msg);
    }
    if(c.cuda_algo_config().active() && c.sum_threshold_algo_config().active()) {
        PANDA_LOG << "rfim::sum_threshold CPU algorithm activated";
        PANDA_LOG << "rfim::gpu sum_threshold algorithm activated";
        _task.template set_algorithms<rfim::cuda::Rfim, RfimBlock<rfim::sum_threshold::Rfim, PolicyType>>(
                             rfim::cuda::Rfim(c.cuda_algo_config()),
                             std::move(RfimBlock<rfim::sum_threshold::Rfim, PolicyType>(c.sum_threshold_algo_config())));
    } 
    if(c.cuda_algo_config().active() && c.ampp_algo_config().active()) {
        PANDA_LOG << "rfim::ampp algorithm activated";
        PANDA_LOG << "rfim::gpu algorithm activated";
        _task.template set_algorithms<rfim::cuda::Rfim, rfim::ampp::Rfim>(
                             rfim::cuda::Rfim(rfim::cuda::Rfim(c.cuda_algo_config())),
                             std::move(rfim::ampp::Rfim(c.ampp_algo_config())));
    } 
    else if(c.cuda_algo_config().active()) {
        PANDA_LOG << "rfim::gpu algorithm activated";
        _task.template set_algorithms<rfim::cuda::Rfim>(std::move(rfim::cuda::Rfim(c.cuda_algo_config())));
    }
    else if(c.ampp_algo_config().active()) {
        // set up ampp task
        PANDA_LOG << "rfim::ampp algorithm activated";
        _task.template set_algorithms<rfim::ampp::Rfim>(std::move(rfim::ampp::Rfim(c.ampp_algo_config())));
    }
    else if(c.sum_threshold_algo_config().active()) {
        PANDA_LOG << "rfim::sum_threshold CPU algorithm activated";
        _task.template set_algorithms<RfimBlock<rfim::sum_threshold::Rfim, PolicyType>>(std::move(rfim::sum_threshold::Rfim(c.sum_threshold_algo_config())));
    }
    else {
        PANDA_LOG << "WARNING: no RFI mitigation algorithm has been specified";
    }
}

template<typename TimeFrequencyType, typename Handler, typename ConfigType>
Rfim<TimeFrequencyType, Handler, ConfigType>::~Rfim()
{
}

template<typename TimeFrequencyType, typename Handler, typename ConfigType>
void Rfim<TimeFrequencyType, Handler, ConfigType>::run(TimeFrequencyType& data) {
    auto ptr = data.shared_from_this();
    _task.submit(ptr);
}

} // namespace rfim
} // namespace cheetah
} // namespace ska

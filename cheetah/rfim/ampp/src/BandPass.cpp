/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/ampp/BandPass.h"
#include "cheetah/rfim/ampp/BandPassConfig.h"
#include "cheetah/utils/BinMap.h"
#include <boost/assert.hpp>
#include <cmath>

namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {


BandPass::BandPass()
{
}

BandPass::BandPass(BandPassConfig const& c)
{
    utils::BinMap<FrequencyType> map(c.number_of_channels());
    map.set_lower_bound(static_cast<FrequencyType>(c.start_frequency()));
    map.set_bin_width(static_cast<FrequencyType>(c.channel_width()));
    set_params(map, c.polynomial_coefficients());
    set_rms(c.rms());
}

BandPass::~BandPass()
{
}

bool BandPass::is_valid() const
{
    return ( _params.size() > 0)
        && ( _primary_map.number_of_bins() > 0 )
        && ( _primary_map.bin_width() != FrequencyType(0.0f * boost::units::si::hertz));
}


const std::vector<double>& BandPass::params() const 
{ 
    return _params;
}

void BandPass::build_data(utils::BinMap<FrequencyType> const& map, float scale)
{
    std::vector<float> data(map.number_of_bins());
    //_data_sets.insert(mapId, QVector<float>(map.numberBins()) );
    for( unsigned int i=0; i < map.number_of_bins(); ++i ) {
        data[i] = scale * evaluate(map.bin_assignment_number(i));
    }
    _data_sets.emplace(std::make_pair(map, std::move(data)));
    BOOST_ASSERT_MSG(_data_sets.find(map) != _data_sets.end(), "BansPass::build_data() : unable to find current map");
}

void BandPass::rebin(utils::BinMap<FrequencyType> const& map)
{
    if(_current_map == map) return;

    _current_map = map;
    BOOST_ASSERT_MSG(map.number_of_bins() != 0, "BandPass::rebin() attempt to set BandPass with 0 bins");
    BOOST_ASSERT_MSG(map.bin_width() != FrequencyType(0.0f * boost::units::si::hertz), "BandPass::rebin() attempt to set bin width to 0Hz");
    if( _data_sets.find(map) == _data_sets.end() ) {
        double scale = map.bin_width()/_primary_map.bin_width();
        _rms[map]= _rms[_primary_map] / std::sqrt( 1.0 / scale );
        _median[map] = _median[_primary_map] * scale;
        _mean[map] = _mean[_primary_map] * scale;
        build_data(map, scale);
    }
}

void BandPass::set_params(utils::BinMap<FrequencyType> const& map, std::vector<double> const& params)
{
    _primary_map = map;
    _current_map = _primary_map;
    _params = params;
    build_data(map, 1.0f);

    BOOST_ASSERT_MSG(map.number_of_bins() != 0, "BandPass::set_params() attempt to set BandPass with 0 bins");
    BOOST_ASSERT_MSG(map.bin_width() != FrequencyType(0.0f * boost::units::si::hertz), "BandPass::set_params() attempt to set bin width to 0Hz");
    BOOST_ASSERT_MSG(_data_sets.find(_primary_map) != _data_sets.end(), "BandPass::set_params(): no primary map");

    // calculate the mean and median
    std::vector<float> copy;
    copy.reserve(map.number_of_bins());

    // calculate rhe mean
    auto& mean = _mean[_primary_map];
    mean = 0.0;
    for( unsigned int i=0; i < map.number_of_bins(); ++i ) {
        auto& data = _data_sets[_primary_map][i];
        mean += data;
        copy.push_back(data);
    }
    mean /= map.number_of_bins();

    // calculate rhe median
    std::nth_element(copy.begin(), copy.begin()+copy.size()/2, copy.end());
    if(copy.size() != 0) {
        _median[_primary_map] = (AmplitudeType)*(copy.begin()+copy.size()/2);
    }
    else {
        _median[_primary_map] = 0.0;
    }
}

void BandPass::set_rms(AmplitudeType const rms)
{
    _rms[_current_map] = rms;
}
  /*
void BandPass::set_median(float const mean) {
    AmplitudeType delta = median - _median[_current_map];
    if( std::fabs(delta) > 0.0f ) {
        // set the new median and rescale the polynomial
        float scale = _current_map.bin_width()/_primary_map.bin_width();
        _median[_current_map] = median;
        _median[_primary_map] = median / scale;
        _params[0] += delta/scale;
        _mean[_current_map] += delta;
        _mean[_primary_map] += delta / scale;
        _data_sets.clear();
        build_data(_current_map, scale);
    }
}
  */
void BandPass::set_mean(float const mean) {
    AmplitudeType delta = mean - _mean[_current_map];
    if( std::fabs(delta) > 0.0f ) {
        // set the new median and rescale the polynomial
        float scale = _current_map.bin_width()/_primary_map.bin_width();
        _params[0] += delta * scale;
        _mean[_current_map] = mean ;
        _mean[_primary_map] = mean / scale ;
        _data_sets.clear();
        build_data(_current_map, scale);
    }
}

std::vector<BandPass::AmplitudeType> const& BandPass::current_set() 
{
    return _data_sets[_current_map]; 
}

BandPass::FrequencyType BandPass::start_frequency() const
{
    return _current_map.lower_bound();
}

BandPass::FrequencyType BandPass::end_frequency() const
{
    return _current_map.upper_bound();
}

BandPass::AmplitudeType BandPass::rms() const { 
    return _rms[_current_map];
}

BandPass::AmplitudeType BandPass::evaluate(FrequencyType const& v) const
{
   AmplitudeType tot = 0.0;
   for(unsigned int i=0; i< _params.size(); ++i ) {
        tot += _params[i]*std::pow(v.value(),i);
   }
   return tot;
}

} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska

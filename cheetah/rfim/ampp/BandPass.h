/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_RFIM_AMPP_BANDPASS_H
#define SKA_CHEETAH_RFIM_AMPP_BANDPASS_H

#include "cheetah/data/Units.h"
#include "cheetah/utils/BinMap.h"
#include <boost/units/systems/si/frequency.hpp>
#include <map>
#include <unordered_map>
#include <vector>

namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {
class BandPassConfig;

/**
 * @brief
 *    Describes a bandpass
 *
 * @details
 *    Takes a description of the bandpass as a polynomial and allows rescaling etc. The polynomial is always linked to a primary utils::BinMap<double> and all conversions are done relative to this primary map.
 */

class BandPass
{
    public:
        typedef data::FrequencyType FrequencyType;
        typedef float AmplitudeType;

    public:
        BandPass();
        BandPass(BandPassConfig const& config);
        BandPass(BandPass const&) = delete;
        BandPass(BandPass&&) = default;
        ~BandPass();

        // default move consignment operator
        BandPass& operator=(BandPass&&) = default;

        // initialise the primary map with a parametric model
        void set_params(utils::BinMap<FrequencyType> const&, std::vector<double> const& params );

        // Set a new rms value for the data
        void set_rms(AmplitudeType const);

        /// Set a new median and rescale the polynomial appropriately. Does not rebin the hash
        void set_median(AmplitudeType const);
        /// Set a new mean and rescale the polynomial appropriately. Does not rebin the hash
        void set_mean(AmplitudeType const);

        /// Return the coefficients of the underlying polynomial
        const std::vector<double>& params() const;

        /// Reset object to use the primary binning map
        //void reset_map();
        //const utils::BinMap<double>& primaryMap() const { return _primaryMap; };

        void rebin(utils::BinMap<FrequencyType> const& map);

        FrequencyType start_frequency() const;
        FrequencyType end_frequency() const;
        //FrequencyType intensity(FrequencyType frequency) const;

        /// Return a reference to the binned data corresponding to the current bin mapping
        const std::vector<AmplitudeType>& current_set();

        /// Return the median for the current bin mapping
        inline AmplitudeType median() const { return _median[_current_map]; }

        /// Return the median for the primary bin mapping
        inline AmplitudeType primary_median() const { return _median[_primary_map]; }

        /// Return the mean for the current bin mapping
        inline AmplitudeType mean() const { return _mean[_current_map]; }

        /// Return the rms for the current bin mapping
        AmplitudeType rms() const;

        /**
         * @brief returns true if the object has been initialised
         */
        bool is_valid() const;

    protected:
        /// Calculate value of parameterised eqn
        AmplitudeType evaluate(FrequencyType const&) const; 

        //void _zeroChannelsMap(const utils::BinMap<double>& map);

        /// Build a data map, scaled appropriately
        void build_data(utils::BinMap<FrequencyType> const& map, float scale);

    private:
        //int _nChannels;
        utils::BinMap<FrequencyType> _primary_map;
        utils::BinMap<FrequencyType> _current_map;
        //std::size_t _currentMapId;
        //std::size_t _primaryMapId;
        std::vector<double> _params;
        FrequencyType _deltaFreq;
        std::unordered_map<utils::BinMap<FrequencyType>, std::vector<AmplitudeType>> _data_sets;
        mutable std::unordered_map<utils::BinMap<FrequencyType>,AmplitudeType> _rms;
        mutable std::unordered_map<utils::BinMap<FrequencyType>,AmplitudeType> _median;
        mutable std::unordered_map<utils::BinMap<FrequencyType>,AmplitudeType> _mean;
};


} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_RFIM_AMPP_BANDPASS_H 

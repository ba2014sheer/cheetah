SUBPACKAGE(ampp)
SUBPACKAGE(cuda)
SUBPACKAGE(sum_threshold)

set(module_rfim_lib_src_cpu
    src/Config.cpp
    src/RfimPolicyLastUnflagged.cpp
    src/FlaggedData.cpp
    src/Metrics.cpp
    ${lib_src_cpu}
    PARENT_SCOPE
)

set(module_rfim_lib_src_cuda
    ${lib_src_cuda}
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)

/**
\anchor Main
\mainpage Introduction

This document describes the Cheetah project and its API (Application Programming Interface).

\section main_whatis What is Cheetah?

Cheetah is a set of computational pipelines for processing beamformed data from radio telescopes.
It has been developed as a prototype for the SKA non imaging pipeline.
*/

- @subpage apps Cheetah Applications

\section main_developers Documents for Developers
- @subpage architecture Architecture Documents
- @ref dev_guide Developers Guide
- @ref code_review_guide Guide for Code Reviewers

/** @defgroup core Cheetah modules */
/** @defgroup test_utils Cheetah Test utility modules */
/** @defgroup architecture Cheetah Architecture Descriptions */
/** @defgroup apps Applications */
